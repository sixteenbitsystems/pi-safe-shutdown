# Pi Safe Shutdown

https://www.sudomod.com/forum/viewtopic.php?f=22&t=2087&p=22341&hilit=simple#p22341

## Wiring

Wire a button to GPIO 03 (pin 5) and Ground (pin 6). For a hard reset button, wire a button into the pad labeled "RUN" and also to a ground.

## Usage

Press button for 2 seconds to shutdown and then press button again for boot.

## Automated Software Install

Go to raspberry command prompt or SSH.

Make sure you are in the home directory by typing ```cd ~ ``` and then type:

```bash
wget https://gitlab.com/sixteenbitsystems/pi-safe-shutdown/raw/master/InstallSafeShutdown.sh
```

Then type:

```bash
git clone https://gitlab.com/sixteenbitsystems/pi-safe-shutdown.git
```

Then type:

```bash
sudo chmod +x InstallSafeShutdown.sh
```
And then type:

```bash
sudo ./InstallSafeShutdown.sh
```

Finally reboot to have it all start on boot with:

```bash
sudo reboot
```