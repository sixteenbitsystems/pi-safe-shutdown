#!/bin/bash

cd ~

sudo apt-get install update
sudo apt-get install python-gpiozero -y
# sudo apt-get install python3-gpiozero -y

sudo chmod 777 /home/pi/pi-safe-shutdown/Start.py
sudo sed -i '/\"exit 0\"/!s/exit 0/\python /home\/pi\/pi-safe-shutdown\/Start.py \&\nexit 0/g' /etc/rc.local